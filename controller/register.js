const express = require('express');
const app = express();
const passport = require('passport');
const bcrypt = require('bcrypt')

const userDB = require('../database/userDB');
const { MongoClient } = require('mongodb');
const { connect } = require('mongoose');

const url = "mongodb+srv://fraydolix:wypD6ISTs5woEgxw@cluster0.c9yip.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

exports.register = (req, res) => {

    bcrypt.hash(req.body.password, 10) 
    .then(function(hash) {
        
        const addUser = new userDB({
            firstName: req.body.firstname,
            lastName: req.body.lastname,
            pseudo: req.body.pseudo,
            email: req.body.email,
            password: hash

        });

        addUser.save()

        .then(() => res.status(201).redirect('/'))
        .catch(err => res.status(400).json({err}));

    })

    .catch(err => res.status(400).json({err}))

}