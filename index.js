const http = require('http');
const express = require('express')
const app = express();

const server = http.createServer(app);
const port = process.env.PORT || 8000;
server.listen(port);

const authRoute = require('./routes/auth')

const ejs = require('ejs')

const { MongoClient } = require('mongodb');
const mongoose = require('mongoose');

const url = "mongodb+srv://fraydolix:wypD6ISTs5woEgxw@cluster0.c9yip.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log("Connection to database success !"))
    .catch(() => console.log("Connection to database failed !"));


app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/public'));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use('/auth', authRoute);

app.get('/', (req, res) => {
    res.render('pages/index')
});

app.get('/login', (req, res) => {
    res.render('pages/login')
});

app.get('/register', (req, res) => {
    res.render('pages/register')
});

app.get('/dashboard', (req, res) => {
    res.render('pages/dashboard')
})

app.get('/panel', (req, res) => {
    res.render('pages/panel')
})