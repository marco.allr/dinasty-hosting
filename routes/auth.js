const express = require('express');
const router = express.Router();
const app = express();

const registerModule = require('../controller/register.js');
const loginModule = require('../controller/login');
const askServer = require('../controller/askServer')

router.post('/register', registerModule.register);
router.post('/login', loginModule.login);
router.post('/panel', askServer.demand);

module.exports = router;