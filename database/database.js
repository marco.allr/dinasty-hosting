const mongoose = require('mongoose');
const unique = require('mongoose-unique-validator');

const users = mongoose.Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    pseudo: { type: String, required: true, unique: true, trim: true },
    email: { type: String, required: true, unique: true, trim: true },
    password: { type: String, required: true, trim: true },
    servers: { type: Array },
    coins: { type: Number, default: 0 },
    role: { type: String, default: 'user' }
})

module.exports = mongoose.model('users', users);