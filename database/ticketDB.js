const mongoose = require('mongoose');
const unique = require('mongoose-unique-validator');

const tickets = new mongoose.Schema({
    ticketID: { type: Number, trim: true },
    userMail: { type: String, required: true, trim: true },
    software: { type: String, required: true, trim: true },
    version: { type: String, required: true, trim: true },
    status: { type: String, trime: true },
    description: { type: String, required: true }
})

module.exports = mongoose.model('tickets', tickets);